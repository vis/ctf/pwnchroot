import click
import logging
from .manager import LibcManager

pass_mgr = click.make_pass_decorator(LibcManager, True)

@click.group(help="Manage database of libc files. See subcommands for more information.")
@click.pass_context
def libcdb(ctx):
    ctx.logger = logging.getLogger("libcdb")
    ctx.logger.info("Testing")
    ctx.obj = LibcManager()

@libcdb.command("list", help="List all libc versions and some information about them.")
@click.option("-d", "--downloaded", help="Only show libc files that are downloaded.", default=False, is_flag=True)
@click.pass_context
def list_libcs(ctx, downloaded):
    pass

@libcdb.command("find", help="Find all possible archives for libcs.")
@pass_mgr
def find_archives(mgr : LibcManager):
    # ctx.mgr : LibcManager
    mgr.find_archives()
    mgr.save_db()

@libcdb.command("download", help="Download archives.")
@click.option("-f", "--force", help="Force download of all archives.", default=False, is_flag=True)
@pass_mgr
def download_archives(mgr : LibcManager, force):
    mgr.download_archives(force)
    mgr.save_db()

@libcdb.command("extract", help="Extract archives.")
@click.option("-f", "--force", help="Force extract of all archives.", default=False, is_flag=True)
@pass_mgr
def download_archives(mgr : LibcManager, force):
    mgr.extract_archives(force)
    mgr.save_db()

@libcdb.command("gen", help="Generate the actual database.")
@pass_mgr
def generate(mgr : LibcManager):
    mgr.generate()