import logging
from .libc_file import LibcFile, DebugLibcFile, ReleaseLibcFile

class Libc(object):
    """This class represents a libc package.
    This does not correspond to the libc version string you see, but rather this is a specific compiled instance of the libc.
    For example two different instances of this class could represent the same libc version (2.23), but from different distros.

    This mainly bundles two `LibcFile`s together, namely the release and debug versions.
    This is mostly due to the fact, that the debug version does not contain any actual data or code.
    This means, we have to keep both of them around.

    Release and debug version are matched together using the build id extracted from both.
    
    Parameters
    ----------
    object : [type]
        [description]

    Attributes
    ----------
    release : LibcFile
        Release version of this libc
    debug : LibcFile
        Debug version of this libc
    logger : logging.Logger
        Logger used to print out important info
    """
    def __init__(self):
        self.logger = logging.getLogger("libcdb.libc")