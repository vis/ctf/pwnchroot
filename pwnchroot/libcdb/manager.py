import logging
from .libc import Libc
from .libc_file import LibcFile, DebugLibcFile, ReleaseLibcFile
import os
import yaml
import requests
from bs4 import BeautifulSoup
import re
from launchpadlib.launchpad import Launchpad
import click
import urllib.request
from concurrent.futures.thread import ThreadPoolExecutor
import shutil
import arpy
from pwnlib.elf import elf

class LibcManager(object):
    """This class is responsible for managing all the different libc files, versions, etc.
    
    Parameters
    ----------
    object : [type]
        [description]

    Attributes
    ----------
    db_path: str
        Path to the database file (which is really just a yaml file)
    files_path: str
        Path where downloaded files will be stored, such as archives and extracted libcs.
        This is computed based on `db_path`.
    libcs: [Libc]
        The different libcs that are currently loaded.
    libc_files: [LibcFile]
        The different libc files that are currently loaded (might not be assigned to a libc yet!).
    launchpad : Launchpad
        API object to interface with launchpad
    """
    def __init__(self, db_path: str = os.path.join(os.getcwd(), "db.yml")):
        self.logger = logging.getLogger("libcdb.manager")
        self.libc_files : [LibcFile] = []
        self.db_path = db_path
        self.files_path = os.path.join(os.path.dirname(self.db_path), "files")
        if not os.path.exists(self.files_path):
            os.mkdir(self.files_path)
        self.load_db()
        self.launchpad = Launchpad.login_anonymously('asdf', 'production', version='devel')

    def load_db(self):
        """Load the database.
        """
        self.logger.info("Loading database from %s", self.db_path)
        root : dict = {}
        with open(self.db_path) as f:
            root = yaml.load(f)
            if root is None:
                root = {}

        if "libc_files" in root:
            for f in root["libc_files"]:
                self.libc_files.append(LibcFile.load(f))


    def save_db(self):
        #self.logger.info("Writing database to %s", self.db_path)
        # make backup!
        bak_path = self.db_path + ".bak"
        if os.path.exists(bak_path):
            os.unlink(bak_path)
        shutil.copy(self.db_path, bak_path)
        root : dict = {}
        libc_files = []
        for f in self.libc_files:
            libc_files.append(f.dump())
        root["libc_files"] = libc_files
        with open(self.db_path, "w") as f:
            yaml.dump(root, f)

    def find_archives(self):
        """Find libc archives among different linux distributions.
        Currently, the following distros are searched:

        ubuntu
            In ubuntu we search the following urls for any packages matching the regex TODO:

            - http://{archive,security}.ubuntu.com/ubuntu/pool/main/g/glibc/
            - http://{archive,security}.ubuntu.com/ubuntu/pool/main/e/eglibc/
            - https://packages.ubuntu.com/{version}/{arch}/{pkg}/download (special)
        """
        self.logger.info("Finding archives...")
        regex = r"libc6(-dbg|-i386|-amd64)?_.*(amd64|i386).deb"
        ubuntu = self.launchpad.distributions["ubuntu"]
        self.libc_files = []
        self.libc_files.extend(self.find_archives_on_lp(ubuntu, "glibc"))
        self.libc_files.extend(self.find_archives_on_lp(ubuntu, "eglibc"))

    def find_archives_on_lp(self, distro, source: str):
        """Find archives located on launchpad, by using the api to query the distro for the source package and builds.
        
        Parameters
        ----------
        distro : [type]
            [description]
        source : str
            [description]
        """
        self.logger.info("Finding archives from source %s", source)
        package_urls = set()
        builds = distro.getBuildRecords(source_name=source)
        reg = re.compile(r".*libc6(-dbg|-i386|-amd64)?_.*(amd64|i386).deb")
        with click.progressbar(builds, label="Going through all builds", show_pos=True) as bar:
            for build in bar:
                hist = build.getLatestSourcePublication()
                urls = hist.binaryFileUrls()
                for url in urls:
                    if reg.match(url) and url not in package_urls:
                        package_urls.add(url)                
        ret = []
        for url in package_urls:
            if "dbg" in url:
                ret.append(DebugLibcFile(url))
            else:
                ret.append(ReleaseLibcFile(url))
        self.logger.info("Found %d packages", len(ret))
        return ret



    def find_archives_on_page(self, url: str, regex: str) -> [LibcFile]:
        """Find any libc archives matching regex on url.
        
        Parameters
        ----------
        url : str
            [description]
        regex : str
            [description]

        Returns
        -------
        [LibcFile]
            Array of all libc files we could find.
        """
        self.logger.debug("Finding archives on %s matching %s", url, regex)

        res = requests.get(url)
        contents = res.text
        bs = BeautifulSoup(contents)
        links = bs.find_all("a")
        ret = []
        reg = re.compile(regex)

        for link in links:
            package_name = link.text
            if reg.match(package_name):
                package_url = url + package_name
                if "dbg" in package_name:
                    ret.append(DebugLibcFile(package_url))
                else:
                    ret.append(ReleaseLibcFile(package_url))

        self.logger.debug("Found %d packages", len(ret))
        return ret

    def download_archives(self, force = False):
        self.logger.info("Downloading archives...")
        path = os.path.join(self.files_path, "debs")
        if not os.path.exists(path):
            os.mkdir(path)
        with click.progressbar(self.libc_files, label="Downloading all debs", show_pos=True) as bar:
            ex = ThreadPoolExecutor(32)
            def download_wrapper(f):
                f.download(path, force)
                return f
            iterator = ex.map(download_wrapper, self.libc_files)
            for f in iterator:
                bar.label = f"Downloaded {f.archive_name}"
                bar.update(1)
                #self.save_db()
        self.logger.info("Downloaded all archives!")
        self.save_db()

    def extract_archives(self, force = False):
        self.logger.info("Extracting archives...")
        download_path = os.path.join(self.files_path, "debs")
        path = os.path.join(self.files_path, "libs")
        if not os.path.exists(path):
            os.mkdir(path)
        new_files = []
        with click.progressbar(self.libc_files, label="Extracting all debs", show_pos=True) as bar:
            for f in bar:
                no_ext = os.path.splitext(f.archive_name)[0]
                archive_path = os.path.join(path, no_ext)
                if not os.path.exists(archive_path):
                    os.mkdir(archive_path)
                try:
                    new_files.extend(f.extract(archive_path))
                except arpy.ArchiveAccessError as e:
                    self.logger.warn("Had an error trying to extract the archive, trying to download again: %s", e)
                    f.download(download_path, True)
                    new_files.extend(f.extract(archive_path))
                bar.label = f"Extracted {f.archive_name}"
        self.libc_files = new_files
        self.save_db()
        self.logger.info("Extracted all archives!")

    def generate(self):
        self.logger.info("Generating actual database...")
        libcs : dict[str, LibcFile] = {}
        for f in self.libc_files:
            if isinstance(f, ReleaseLibcFile):
                f.load_elf()
                build_id = f.build_id
                if build_id is not None:
                    self.logger.debug("Found build_id %s for archive %s", build_id, f.archive_name)
                    if build_id in libcs:
                        prev = libcs[build_id]
                        self.logger.warn("Already added previous file %s for build id %s (%s)", prev.archive_name, build_id, f.archive_name)
                    else:
                        libcs[build_id] = f
                else:
                    self.logger.warn("Archive %s has no build id!", f.archive_name)
