import logging
import pwnlib.elf as elf
import urllib.request
import os
from ..dpkg import Dpkg
import tarfile

class LibcFile(object):
    """This class represents a libc file. It can either be the debug or release version of said file.
    However, it carries a lot more information, than just the file.
    It stores the location where we can download the necessary .deb (or other archive) from.
    It also stores the .deb file and/or extracted .so file (if downloaded and/or extracted).
    
    Parameters
    ----------
    object : [type]
        [description]

    Attributes
    ----------
    archive_url : str
        Location where the archive containing this libc was downloaded from.
        This is usually a link to the .deb on some distro mirror.
    archive_path : str
        Path to the downloaded archive file, if it was downloaded.
    archive_name : str
        Filename of the archive file.
    elf_path : str
        Path to the extracted libc file, if it was extracted.
    elf : elf.ELF
        pwntools ELF object that can contain a lot of information about the library.
    """
    def __init__(self, archive_url: str):
        self.archive_url = archive_url
        self.elf_path = None
        self.archive_name = self.archive_url.split("/")[-1]
        self.archive_path = None
        self.logger = logging.getLogger("libcdb.libc_file")

    def download(self, path, force = False):
        if self.archive_path is None or force:
            self.archive_path = os.path.join(path, self.archive_name)
            urllib.request.urlretrieve(self.archive_url, self.archive_path)

    def extract(self, path):
        self.deb = Dpkg(self.archive_path)
        self.logger.debug("Loaded deb %s with package %s and version %s", self.archive_path, self.deb.get("package"), self.deb.get("version"))
        ret = []
        count = 0
        with self.deb.extract_data() as ctar:
            for member in ctar.getmembers():
                if self.filter_archive_member(member):
                    filename = os.path.basename(member.name)
                    filename = f"{count}-{filename}"
                    self.elf_path = os.path.join(path, filename)
                    comp_f = ctar.extractfile(member)
                    with open(self.elf_path, "wb") as f:
                        f.write(comp_f.read())
                    info = self.dump()
                    count += 1
                    # duplicate this file
                    ret.append(LibcFile.load(info))
        return ret

    def filter_archive_member(self, member: tarfile.TarInfo):
        filename = os.path.basename(member.name)
        filename, ext = os.path.splitext(filename)
        self.logger.debug("Path %s", filename)
        return member.isfile() and "libc-" in filename and ".so" == ext

    def dump(self):
        return {
            "archive_url" : self.archive_url,
            "type" : "release" if isinstance(self, ReleaseLibcFile) else "debug",
            "archive_path" : self.archive_path,
            "elf_path" : self.elf_path
        }

    def load(info):
        f = None
        archive_url = info["archive_url"]
        if info["type"] == "debug":
            f = DebugLibcFile(archive_url)
        else:
            f = ReleaseLibcFile(archive_url)
        f.archive_path = info.get("archive_path", None)
        f.elf_path = info.get("elf_path", None)
        return f

    def load_elf(self):
        """Loads the so file using pwntools for more detailed inspection (such as retrieving the build id).
        """
        if self.elf_path is not None:
            self.elf = elf.ELF(self.elf_path, checksec=False)

    @property
    def build_id(self) -> str:
        """The build id of this so file, formatted with hex.
        This can only be retreived if the elf is loaded, so call `self.load_elf()` before this!
        
        Returns
        -------
        str
            The hex encoded gnu build id.
        """
        if self.elf is not None and self.elf.buildid is not None:
            return self.elf.buildid.hex()
        return None
    

class ReleaseLibcFile(LibcFile):
    """This class represents a release libc file.
    
    Parameters
    ----------
    LibcFile : [type]
        [description]
    """
    pass
    

class DebugLibcFile(LibcFile):
    pass