import click
from .log import setup_logging
import logging

@click.group()
@click.option('--log-level', help='Log level for console. Can specify either an integer or a string.', default='INFO')
@click.pass_context
def main(ctx, log_level):
    setup_logging(log_level)
    logger = logging.getLogger("main")
    logger.info("Hello there, general kenobi")

def start():
    main()

from .libcdb.cli import libcdb

main.add_command(libcdb)

if __name__ == '__main__':
    start()