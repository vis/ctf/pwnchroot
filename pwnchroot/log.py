import logging, coloredlogs

def log_fmt() -> str:
    """Create the basic log format used everywhere
    
    Returns
    -------
    str
        [description]
    """

    return "%(asctime)s - %(name)s[%(funcName)s()@%(filename)s:%(lineno)d] %(levelname)s: %(message)s"


def create_filehandler(filename: str, level: int) -> logging.FileHandler:
    """Create FileHandler based on filename and log level.
    
    Parameters
    ----------
    filename : str
        The filename to save the log to.
    level : int
        The minimum level to log.
    
    Returns
    -------
    logging.FileHandler
        [description]
    """
    fh = logging.FileHandler(filename)
    fh.setLevel(level)

    return fh

def setup_logging(stdout_level):
    """Setup the default logging.
    
    Parameters
    ----------
    stdout_level : [type]
        [description]
    """
    root = logging.getLogger()

    info_fh = create_filehandler("pwnchroot.log", logging.INFO)
    debug_fh = create_filehandler("pwnchroot.debug.log", logging.DEBUG)
    file_fmt = logging.Formatter(log_fmt())
    info_fh.setFormatter(file_fmt)
    debug_fh.setFormatter(file_fmt)

    root.addHandler(info_fh)
    root.addHandler(debug_fh)

    coloredlogs.install(level=stdout_level, fmt=log_fmt(), field_styles={})
    root.setLevel(logging.DEBUG) # try logging everything!
