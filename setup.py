from setuptools import setup, find_packages

setup(
      # mandatory
      name='pwnchroot',
      # mandatory
      version='0.1',
      # mandatory
      author_email='leonardo.galli@vis.ethz.ch',
      packages=find_packages(),
      package_data={},
      install_requires=['click', 'pwntools', 'coloredlogs', 'pyyaml', 'requests', 'bs4', 'launchpadlib', 'arpy', 'six'],
      entry_points={
        'console_scripts': ['pwnchroot = pwnchroot.cli:start']
      }
)